﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private new Camera camera;

    private static GameObject lastUI;

    private const string MESH_NAME = "Mesh";

    private void Start()
    {
        this.camera = Camera.main;
    }

    private void Update()
    {
        HandleMouseInput();
    }

    private void HandleMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SelectHittedShape();
        }
    }

    private void SelectHittedShape()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity) && hit.transform.name.Equals(MESH_NAME))
        {
            SwitchOffPreviowsUI();
            SwitchHitUIOn(hit);
        }
    }

    private static void SwitchOffPreviowsUI()
    {
        if (lastUI)
            lastUI.SetActive(false);
    }

    private static void SwitchHitUIOn(RaycastHit hit)
    {
        hit.transform.gameObject.GetComponentInParent<ShapeMenu>().UI.SetActive(true);

        lastUI = hit.transform.gameObject.GetComponentInParent<ShapeMenu>().UI;
    }
}
