﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseTerrain : PlainTerrain
{
    [Space]
    [Header("Noise levels")]
    [SerializeField] float persistance = 2.0f;
    [SerializeField] float lacunarity = 0.3f;

    protected override float CalculateNoise(int x, int z)
    {
        return Mathf.Sin(Mathf.PerlinNoise(x * lacunarity, z * lacunarity)) * persistance;
    }
}
