﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlainTerrain : MonoBehaviour
{
    [SerializeField] protected int gridWidth = 256;
    [SerializeField] protected int gridHeight = 256;

    private Mesh mesh;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    private Vector3[] vertices;
    private Vector2[] uvs;
    private int[] triangles;

    void Start()
    {
        mesh = new Mesh();

        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        //meshRenderer = GetComponent<MeshRenderer>();
        //meshRenderer.material.shader = Shader.Find("Standard");
        
        Initialize();
        DrawNoiseMap();
    }

    private void OnValidate()
    {
        if (gridWidth < 1)
            gridWidth = 1;
        if (gridHeight < 1)
            gridHeight = 1;

        this.transform.gameObject.name = "LandMassTerrain";
        mesh = new Mesh();

        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        //meshRenderer = GetComponent<MeshRenderer>();
        //meshRenderer.material.shader = Shader.Find("Standard");

        Initialize();
        DrawNoiseMap();
    }

    /*
    private void OnDrawGizmos()
    {
        if (vertices == null)
            return;

        foreach (var vertex in vertices)
        {
            Gizmos.DrawSphere(vertex, 0.1f);
        }
    }*/

    private void Initialize()
    {
        vertices = new Vector3[(gridWidth + 1) * (gridHeight + 1)];
        triangles = new int[(gridWidth) * (gridHeight) * 6];
        uvs = new Vector2[(gridWidth + 1) * (gridHeight + 1)];


        CalculateVertices();
        GenerateTriangles();

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }

    private void GenerateTriangles()
    {
        int index = 0;
        int tri = 0;

        for (int h = 0; h < gridHeight; h++)
        {
            for (int i = 0; i < gridWidth; i++)
            {
                triangles[tri] = index;
                triangles[tri + 1] = gridWidth + index + 1;
                triangles[tri + 2] = index + 1;
                
                triangles[tri + 3] = gridWidth + index + 2;
                triangles[tri + 4] = index + 1;
                triangles[tri + 5] = gridWidth + index + 1;

                ++index;
                tri += 6;
            }
            ++index;
        }
    }

    private void CalculateVertices()
    {
        int i = 0;

        for (int z = 0; z <= gridHeight; z++)
        {
            for (int x = 0; x <= gridWidth; x++)
            {
                float y = CalculateNoise(x, z);

                vertices[i] = new Vector3(x, y, z);
                // Needed to show the texture
                uvs[i] = new Vector2(x, z);

                ++i;
            }
        }
    }

    protected virtual float CalculateNoise(int x, int z)
    {
        return 0.0f;
    }

    protected virtual void DrawNoiseMap()
    {
        return;
    }
}
