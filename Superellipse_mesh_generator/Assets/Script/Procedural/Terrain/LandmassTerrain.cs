﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandmassTerrain : PlainTerrain
{
    [Space]
    [Header("Map setting")]
    [SerializeField] private float noiseScale;

    [Space]
    [Header("Texture settings")]
    [SerializeField] private Renderer textureRenderer;

    [Space]
    [Header("Noise parameters")]
    [Range(0, 100)] [SerializeField] private int ocatves;
    [Range(1.0f, 100.0f)] [SerializeField] private float lacunarity;
    [Range(0.0f, 1.0f)] [SerializeField] private float persistance;
    [SerializeField] private int seed;
    [SerializeField] private Vector2 offset;
    

    //TODO: Check why we render wrongly the pattern, if don't know swap to normal plane
    protected override void DrawNoiseMap()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(gridWidth, gridHeight, noiseScale, ocatves, persistance, lacunarity, seed, offset);

        Texture2D texture = new Texture2D(gridWidth, gridHeight);

        Color[] colorMap = new Color[gridWidth * gridHeight];
        for (int y = 0; y < gridHeight; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                colorMap[y * gridWidth + x] = Color.Lerp(Color.black, Color.white, noiseMap[x, y]);
            }
        }

        texture.SetPixels(colorMap);
        texture.Apply();

        // Shared material allow me to see everythin in the editor
        textureRenderer.sharedMaterial.mainTexture = texture;
        //textureRenderer.material.mainTexture = texture;
        //textureRenderer.transform.localScale = new Vector3(gridWidth, 0, gridHeight);
    }
}
