﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    [SerializeField] protected int gridWidth = 256;
    [SerializeField] protected int gridHeight = 256;

    private Mesh mesh;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    private Vector3[] vertices;
    private Vector2[] uvs;
    private int[] triangles;

    void Start()
    {
        mesh = new Mesh();

        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        Initialize();
        DrawNoiseMap();
    }

    private void OnValidate()
    {
        this.transform.gameObject.name = "Plane";
        mesh = new Mesh();

        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        Initialize();
        DrawNoiseMap();
    }

    private void Initialize()
    {
        vertices = new Vector3[4];
        triangles = new int[6];
        uvs = new Vector2[4];


        CalculateVertices();
        GenerateTriangles();

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }

    private void GenerateTriangles()
    {
        triangles[0] = 0;
        triangles[1] = 2;
        triangles[2] = 1;

        triangles[3] = 3;
        triangles[4] = 1;
        triangles[5] = 2;
    }

    private void CalculateVertices()
    {

        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(gridWidth, 0, 0);
        vertices[2] = new Vector3(0, 0, gridHeight);
        vertices[3] = new Vector3(gridWidth, 0, gridHeight);

        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(gridWidth, 0);
        uvs[2] = new Vector2(0, gridHeight);
        uvs[3] = new Vector2(gridWidth, gridHeight);
    }

    protected virtual float CalculateNoise(int x, int z)
    {
        return 0.0f;
    }

    protected virtual void DrawNoiseMap()
    {
        return;
    }
}
