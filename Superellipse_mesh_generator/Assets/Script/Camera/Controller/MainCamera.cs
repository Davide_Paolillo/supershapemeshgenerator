﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    [SerializeField] private float cameraSpeed;

    private Camera mainCamera;

    private bool lockCamera;

    void Start()
    {
        mainCamera = Camera.main;
        lockCamera = false;
    }

    void Update()
    {
        HandleMouseInputs();
        HandleKeyboardInputs();
    }

    private void HandleMouseInputs()
    {
        var mousePos = Input.mousePosition;
        var yPos = (Mathf.Clamp(mainCamera.ScreenToViewportPoint(mousePos).y, 0, 1) * 2 - 1) * 90.0f;
        if (!lockCamera)
            this.transform.rotation = Quaternion.Euler(-yPos, mousePos.x, 0.0f);
    }

    private void HandleKeyboardInputs()
    {
        if (Input.GetKey(KeyCode.W))
        {
            var forward = new Vector3(mainCamera.transform.forward.x, 0f, mainCamera.transform.forward.z);
            mainCamera.transform.position += (Time.deltaTime * forward * cameraSpeed);
        }

        if (Input.GetKey(KeyCode.S))
        {
            var forward = new Vector3(mainCamera.transform.forward.x, 0f, mainCamera.transform.forward.z);
            mainCamera.transform.position += (Time.deltaTime * -forward * cameraSpeed);
        }

        if (Input.GetKey(KeyCode.A))
        {
            var left = new Vector3(mainCamera.transform.right.x, 0f, mainCamera.transform.right.z);
            mainCamera.transform.position += (Time.deltaTime * -left * cameraSpeed);
        }

        if (Input.GetKey(KeyCode.D))
        {
            var left = new Vector3(mainCamera.transform.right.x, 0f, mainCamera.transform.right.z);
            mainCamera.transform.position += (Time.deltaTime * left * cameraSpeed);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            mainCamera.transform.position += (Time.deltaTime * Vector3.up * cameraSpeed);
        }

        if (Input.GetKey(KeyCode.X))
        {
            var down = (Time.deltaTime * -Vector3.up * cameraSpeed);
            mainCamera.transform.position += (Time.deltaTime * -Vector3.up * cameraSpeed);
            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x, Mathf.Clamp(mainCamera.transform.position.y, 0.5f, Mathf.Infinity), mainCamera.transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.CapsLock))
            lockCamera = !lockCamera;

        if (Input.GetKeyDown(KeyCode.Tab))
            lockCamera = !lockCamera;
    }
}
