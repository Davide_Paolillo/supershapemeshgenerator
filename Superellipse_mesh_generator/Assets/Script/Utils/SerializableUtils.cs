﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class SerializableUtils
{
    public string vertices;
    public string triangles;

    public SerializableUtils(Utils utils)
    {
        //vertices = "";
        //triangles = "";

        vertices = String.Join("", utils.vertices);
        triangles = String.Join(", ", utils.triangles); 

        /*foreach (Vector3 vec3 in utils.vertices)
        {
            vertices += "[" + vec3.x.ToString() + ", " + vec3.y.ToString() + ", " + vec3.z.ToString() + "] \n";
        }

        for (int i = 0; i < utils.triangles.Length; i += 6)
        {
            triangles += "[" + triangles[i] + ", " + triangles[i + 1] + ", " + triangles[i + 2] +
                ", " + triangles[i + 3] + ", " + triangles[i + 4] + ", " + triangles[i + 5] + "] \n";
        }*/
    }
}
