﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{
    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, float scale, int octaves, float persistance, float lacunarity, int seed, Vector2 offset)
    {

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        // Spawn octaves at random locations, so we get a more complex pattern
        for(int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) + offset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        float[,] noiseMap = new float[mapWidth, mapHeight];

        // Get center coordinates to zoom the pattern at center when we increase the noise scale
        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        if (scale <= 0)
        {
            scale = Mathf.Epsilon;
        }

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    // Multipling for frequency, that indicates the variation in the y value, at high values correspond to further points, and so indicates high value changhing and so do the noise.
                    float sampleX = (x - halfWidth) / scale * frequency + octaveOffsets[i].x;
                    float sampleY = (y - halfHeight) / scale * frequency + octaveOffsets[i].y;

                    // This way we double the result then we subtract the value (0 <= perlinNoise <= 0.707 if we pass only positive values), so by doubling it and subtracting one will result also in negarive values
                    float perilnValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    // At each iteration sum the previous results with the new perlin noise level, multipied for the frequence of it in the x axis (amplitude)
                    noiseHeight += perilnValue * amplitude;
                    noiseMap[x, y] = perilnValue;

                    // 0 <= persistance <= 1 so we are effectively decreasing the value at each iteration
                    amplitude *= persistance;
                    // lacunarity > 1 so we are effectively increasing the value at each iteration
                    frequency *= lacunarity;
                }

                noiseMap[x, y] = noiseHeight;
            }
        }

        NormalizeMap(mapHeight, mapWidth, ref noiseMap);

        return noiseMap;
    }

    private static void NormalizeMap(int mapHeight, int mapWidth, ref float[,] map)
    {
        float maxValue = map.Cast<float>().Max();
        float minValue = map.Cast<float>().Min();

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                map[x, y] = Mathf.InverseLerp(minValue, maxValue, map[x, y]);
            }
        }
    }

}
