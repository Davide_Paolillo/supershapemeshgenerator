﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject superShape4D;
    [SerializeField] private GameObject parametricSuperShape3D;
    [SerializeField] private GameObject superShape3D;
    [SerializeField] private GameObject superEgg;

    private List<SerializableUtils> serializableUtils;
    private DataStorageHandler<List<SerializableUtils>> dataStorageHandler;

    private void Start()
    {
        serializableUtils = new List<SerializableUtils>();
        dataStorageHandler = new DataStorageHandler<List<SerializableUtils>>();
    }

    public void ToggleSuperShape4D()
    {
        superShape4D.SetActive(!superShape4D.activeInHierarchy);
    }

    public void ToggleParametricSuperShape3D()
    {
        parametricSuperShape3D.SetActive(!parametricSuperShape3D.activeInHierarchy);
    }

    public void ToggleSuperShape3D()
    {
        superShape3D.SetActive(!superShape3D.activeInHierarchy);
    }

    public void ToggleSuperEgg()
    {
        superEgg.SetActive(!superEgg.activeInHierarchy);
    }

    public void SaveToFile()
    {
        serializableUtils.Clear();

        foreach (var kw in SuperShape.cacheDictionary)
        {
            SerializableUtils serializableUtil = new SerializableUtils(kw.Value);
            serializableUtils.Add(serializableUtil);
        }
        
        dataStorageHandler.SaveToTextInFile(serializableUtils);
    }
}
