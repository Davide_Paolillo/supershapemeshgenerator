﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GeneralizedSuperShape3DUI : ShapeUI
{
    [Header("Sliders")]
    [SerializeField] private Slider n1;
    [SerializeField] private Slider n2;
    [SerializeField] private Slider n3;
    [SerializeField] private Slider theta;
    [SerializeField] private Slider delta;

    [Header("InputFields")]
    [SerializeField] private TMP_InputField inputN1;
    [SerializeField] private TMP_InputField inputN2;
    [SerializeField] private TMP_InputField inputN3;
    [SerializeField] private TMP_InputField inputTheta;
    [SerializeField] private TMP_InputField inputDelta;

    private GeneralizedSuperShape3D generalizedSuperShape3D;

    private float[] parameters;

    private void Start()
    {
        generalizedSuperShape3D = this.transform.parent.GetComponent<GeneralizedSuperShape3D>();
        parameters = new float[7];
        parameters[0] = a.value;
        parameters[1] = b.value;
        parameters[2] = n1.value;
        parameters[3] = n2.value;
        parameters[4] = n3.value;
        parameters[5] = theta.value;
        parameters[6] = delta.value;
        InitializeSliderValues();
    }

    private void InitializeSliderValues()
    {
        a.value = generalizedSuperShape3D.A;
        b.value = generalizedSuperShape3D.B;
        n1.value = generalizedSuperShape3D.N1;
        n2.value = generalizedSuperShape3D.N2;
        n3.value = generalizedSuperShape3D.N3;
        theta.value = generalizedSuperShape3D.Zeta;
        delta.value = generalizedSuperShape3D.Delta;

        UpdateValuesInInputFields();
    }

    private void UpdateValuesInInputFields()
    {
        inputA.text = a.value.ToString();
        inputB.text = b.value.ToString();
        inputN1.text = n1.value.ToString();
        inputN2.text = n2.value.ToString();
        inputN3.text = n3.value.ToString();
        inputTheta.text = theta.value.ToString();
        inputDelta.text = delta.value.ToString();
    }

    protected override void UpdateShape()
    {
        float[] newValues = { a.value, b.value, delta.value, theta.value, n1.value, n2.value, n3.value };

        if (!newValues.SequenceEqual<float>(parameters))
        {
            generalizedSuperShape3D
                .GenerateMeshWithParameters(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4], newValues[5], newValues[6]);
            Array.Copy(newValues, parameters, newValues.Length);
        }
    }

    #region UI_UPDATERS
    public void UpdateN1ValueFromFloat(float value)
    {
        if (n1)
            n1.value = value;
        if (inputN1)
            inputN1.text = value.ToString();
    }

    public void UpdateN1ValueFromString(string value)
    {
        if (n1)
            n1.value = float.Parse(value);
        if (inputN1)
            inputN1.text = value;
    }

    public void UpdateN2ValueFromFloat(float value)
    {
        if (n2)
            n2.value = value;
        if (inputN2)
            inputN2.text = value.ToString();
    }

    public void UpdateN2ValueFromString(string value)
    {
        if (n2)
            n2.value = float.Parse(value);
        if (inputN2)
            inputN2.text = value;
    }

    public void UpdateN3ValueFromFloat(float value)
    {
        if (n3)
            n3.value = value;
        if (inputN3)
            inputN3.text = value.ToString();
    }

    public void UpdateN3ValueFromString(string value)
    {
        if (n3)
            n3.value = float.Parse(value);
        if (inputN3)
            inputN3.text = value;
    }

    public void UpdateThetaValueFromFloat(float value)
    {
        if (theta)
            theta.value = value;
        if (inputTheta)
            inputTheta.text = value.ToString();
    }

    public void UpdateThetaValueFromString(string value)
    {
        if (theta)
            theta.value = float.Parse(value);
        if (inputTheta)
            inputTheta.text = value;
    }

    public void UpdateDeltaValueFromFloat(float value)
    {
        if (delta)
            delta.value = value;
        if (inputDelta)
            inputDelta.text = value.ToString();
    }

    public void UpdateDeltaValueFromString(string value)
    {
        if (delta)
            delta.value = float.Parse(value);
        if (inputDelta)
            inputDelta.text = value;
    }
    #endregion
}
