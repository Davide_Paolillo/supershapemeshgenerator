﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SuperEggUI : ShapeUI
{
    [Header("Sliders")]
    [SerializeField] private Slider c;
    [SerializeField] private Slider r;
    [SerializeField] private Slider t;

    [Header("InputFields")]
    [SerializeField] private TMP_InputField inputC;
    [SerializeField] private TMP_InputField inputR;
    [SerializeField] private TMP_InputField inputT;

    private SuperEgg superEgg;

    private float[] parameters;

    private void Start()
    {
        superEgg = this.transform.parent.GetComponent<SuperEgg>();
        parameters = new float[5];
        parameters[0] = a.value;
        parameters[1] = b.value;
        parameters[2] = c.value;
        parameters[3] = t.value;
        parameters[4] = r.value;
        InitializeSliderValues();
    }

    private void InitializeSliderValues()
    {
        a.value = superEgg.A;
        b.value = superEgg.B;
        c.value = superEgg.C;
        r.value = superEgg.R;
        t.value = superEgg.T;

        UpdateValuesInInputFields();
    }

    private void UpdateValuesInInputFields()
    {
        inputA.text = a.value.ToString();
        inputB.text = b.value.ToString();
        inputC.text = c.value.ToString();
        inputR.text = r.value.ToString();
        inputT.text = t.value.ToString();
    }

    protected override void UpdateShape()
    {
        float[] newValues = { a.value, b.value, c.value, r.value, t.value };

        if (!newValues.SequenceEqual<float>(parameters))
        {
            superEgg.GenerateMeshWithParameters(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4]);
            Array.Copy(newValues, parameters, newValues.Length);
        }
    }

    #region UI_UPDATERS
    public void UpdateCValueFromFloat(float value)
    {
        if (c)
            c.value = value;
        if (inputC)
            inputC.text = value.ToString();
    }

    public void UpdateCValueFromString(string value)
    {
        if (c)
            c.value = float.Parse(value);
        if (inputC)
            inputC.text = value;
    }

    public void UpdateRValueFromFloat(float value)
    {
        if (r)
            r.value = value;
        if (inputR)
            inputR.text = value.ToString();
    }

    public void UpdateRValueFromString(string value)
    {
        if (r)
            r.value = float.Parse(value);
        if (inputR)
            inputR.text = value;
    }

    public void UpdateTValueFromFloat(float value)
    {
        if (t)
            t.value = value;
        if (inputT)
            inputT.text = value.ToString();
    }

    public void UpdateTValueFromString(string value)
    {
        if (t)
            t.value = float.Parse(value);
        if (inputT)
            inputT.text = value;
    }
    #endregion
}
