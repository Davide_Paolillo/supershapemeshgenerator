﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class ShapeUI : MonoBehaviour
{
    [SerializeField] protected Slider a;
    [SerializeField] protected Slider b;

    [SerializeField] protected TMP_InputField inputA;
    [SerializeField] protected TMP_InputField inputB;

    private void Update()
    {
        UpdateShape();
    }

    protected abstract void UpdateShape();

    #region UI_UPDATERS
    public void UpdateAValueFromFloat(float value)
    {
        if (a)
            a.value = value;
        if (inputA)
            inputA.text = value.ToString();
    }

    public void UpdateAValueFromString(string value)
    {
        if (a)
            a.value = float.Parse(value);
        if (inputA)
            inputA.text = value;
    }

    public void UpdateBValueFromFloat(float value)
    {
        if (b)
            b.value = value;
        if (inputA)
            inputB.text = value.ToString();
    }

    public void UpdateBValueFromString(string value)
    {
        if (b)
            b.value = float.Parse(value);
        if (inputA)
            inputB.text = value;
    }
    #endregion
}
