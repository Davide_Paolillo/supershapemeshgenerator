﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    [Range(0.0f, 100.0f)][SerializeField] private float radius = 1.0f;

    private MeshFilter meshFilter;
    private Mesh mesh;
    private MeshRenderer meshRenderer;

    // Start is called before the first frame update
    void Start()
    {
        meshFilter = this.gameObject.AddComponent<MeshFilter>();
        mesh = meshFilter.mesh;
        meshRenderer = this.gameObject.AddComponent<MeshRenderer>();

        IcoSphere.Create(this.gameObject, radius);

        mesh.RecalculateBounds();
        mesh.RecalculateTangents();
        mesh.RecalculateNormals();
    }

    // Update is called once per frame
    void Update()
    {
        IcoSphere.Create(this.gameObject, radius);

        mesh.RecalculateBounds();
        mesh.RecalculateTangents();
        mesh.RecalculateNormals();
    }
}
