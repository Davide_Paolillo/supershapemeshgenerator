﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomQuadMesh : MonoBehaviour
{
    [SerializeField] private int width = 1;
    [SerializeField] private int height = 1;

    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer meshRenderer = this.gameObject.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));

        MeshFilter meshFilter = this.gameObject.AddComponent<MeshFilter>();
        Mesh mesh = TriangleMeshGenerator.GetQuadMesh(width, height);

        meshFilter.mesh = mesh;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
