﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperShape2D : MonoBehaviour
{
    [Range(0.0f, 100.0f)] [SerializeField] private float radius = 1.0f;

    [Range(0, 100)] [SerializeField] private float numberOfPoints = 64;

    private MeshFilter meshFilter;
    private Mesh mesh;
    private MeshRenderer meshRenderer;

    const float PI = Mathf.PI;
    const float TWO_PI = Mathf.PI * 2.0f;
    const float HALF_PI = Mathf.PI / 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        for(int lt = 0; lt < numberOfPoints; lt++)
        {
            float latitude = Map(lt, 0, numberOfPoints, 0, PI);
            for (int ln = 0; ln < numberOfPoints; ln++)
            {
                float longitude = Map(ln, 0, numberOfPoints, 0, TWO_PI);

                float x_pos = radius * (float)Mathf.Cos(latitude) * (float)Math.Cos(longitude);
                float y_pos = radius * Mathf.Sin(latitude) * (float)Math.Cos(longitude);
                float z_pos = radius * (float)Math.Cos(longitude);
            }
        }

        meshFilter = this.gameObject.AddComponent<MeshFilter>();
        mesh = meshFilter.mesh;
        meshRenderer = this.gameObject.AddComponent<MeshRenderer>();

        IcoSphere.Create(this.gameObject, radius);
    }

    private float Map(float val, float currentStart, float currentEnd, float targetStart, float targetEnd)
    {
        float targetRange = targetEnd - targetStart;
        float currentRange = currentEnd - currentStart;
        float rangeIncrements = targetRange / currentRange;

        return targetStart + (rangeIncrements * (val - currentStart));
    }

    // Update is called once per frame
    void Update()
    {
        IcoSphere.Create(this.gameObject, radius);
    }
}
