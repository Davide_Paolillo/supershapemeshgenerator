﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Utilities class to compute supershapes components
/// </summary>
public static class SuperSpaheMath
{
    /// <summary>
    /// Mapping utility function to get the right longitude or latitude with the given parameters
    /// </summary>
    /// <param name="val">Latitude or longitude value</param>
    /// <param name="currentStart">Initial resolution (ideally should be 0)</param>
    /// <param name="currentEnd">Final resolution to achieve</param>
    /// <param name="targetEnd">Lower angle value possible for the latitude or longitude</param>
    /// <param name="targetStart">Higher angle value possible for the latitude or the longitude</param>
    /// <returns></returns>
    public static float Map(float val, float currentStart, float currentEnd, float targetEnd, float targetStart)
    {
        float targetRange = targetEnd - targetStart;
        float currentRange = currentEnd - currentStart;
        float rangeIncrements = targetRange / currentRange;

        return targetStart + (rangeIncrements * (val - currentStart));
    }

    /// <summary>
    /// Utility function that computes the radius of a super formula with the given parameters
    /// </summary>
    /// <param name="theta">The angle in radiants - might be a value of latitude or longitude</param>
    /// <param name="a">Real number != 0</param>
    /// <param name="b">Real number != 0</param>
    /// <param name="m">Real number, adds rotational simmetry</param>
    /// <param name="n1">Real number</param>
    /// <param name="n2">Real number</param>
    /// <param name="n3">Real number</param>
    /// <returns></returns>
    public static float ComputeSuperShape(float theta, float a, float b, float m, float n1, float n2, float n3)
    {
        float addend1 = (1 / a) * Mathf.Cos((m * theta) / 4);
        float addend2 = (1 / b) * Mathf.Sin((m * theta) / 4);
        addend1 = Mathf.Pow(Mathf.Abs(addend1), n2);
        addend2 = Mathf.Pow(Mathf.Abs(addend2), n3);

        float radius = Mathf.Pow((addend1 + addend2), (-1) / n1);

        return radius;
    }

    /// <summary>
    /// Utility function that computes the radius of a generalized super formula with the given parameters
    /// </summary>
    /// <param name="theta">The angle in radiants - might be a value of latitude or longitude</param>
    /// <param name="a">Real number != 0</param>
    /// <param name="b">Real number != 0</param>
    /// <param name="delta">Real number, adds rotational simmetry</param>
    /// <param name="zeta">Real number, adds rotational simmetry</param>
    /// <param name="n1">Real number</param>
    /// <param name="n2">Real number</param>
    /// <param name="n3">Real number</param>
    /// <returns></returns>
    public static float ComputeGeneralizedShape(float theta, float a, float b, float delta, float zeta, float n1, float n2, float n3)
    {
        float addend1 = (1 / a) * Mathf.Cos((delta * theta) / 4);
        float addend2 = (1 / b) * Mathf.Sin((zeta * theta) / 4);
        addend1 = Mathf.Pow(Mathf.Abs(addend1), n2);
        addend2 = Mathf.Pow(Mathf.Abs(addend2), n3);

        float radius = Mathf.Pow((addend1 + addend2), (1) / n1);

        return radius;
    }

    /// <summary>
    /// Computing the auxiliarry cos, just a mathematical utils
    /// </summary>
    /// <param name="theta">The angle in radiants</param>
    /// <param name="m">The vertical/horizontal radius already computed</param>
    /// <returns></returns>
    public static float ComputeAuxiliaryCos(float theta, float m)
    {
        float c = Mathf.Cos(theta);

        return Mathf.Sign(c) * Mathf.Pow(Mathf.Abs(c), m);
    }

    /// <summary>
    /// Computing the auxiliarry sin, just a mathematical utils
    /// </summary>
    /// <param name="theta">The angle in radiants</param>
    /// <param name="m">The vertical/horizontal radius already computed</param>
    /// <returns></returns>
    public static float ComputeAuxiliarySin(float theta, float m)
    {
        float s = Mathf.Sin(theta);

        return Mathf.Sign(s) * Mathf.Pow(Mathf.Abs(s), m);
    }
}
