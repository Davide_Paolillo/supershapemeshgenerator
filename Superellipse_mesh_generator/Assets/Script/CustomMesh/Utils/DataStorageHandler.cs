﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

public class DataStorageHandler<T>
{
    public void SaveToFile(T log)
    {
        string destination = Application.persistentDataPath + "/data.gielis";
        FileStream file;

        if (File.Exists(destination))
            file = File.OpenWrite(destination);
        else
            file = File.Create(destination);

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(file, log);
        file.Close();
    }

    public void SaveToTextInFile(List<SerializableUtils> log)
    {
        string destination = Application.persistentDataPath + "/data.txt";
        StreamWriter streamWriter = new StreamWriter(destination, false);
        foreach (SerializableUtils value in log)
        {
            streamWriter.WriteLine("************************************************************************************************* VERTICES ************************************************************************************************************************************************************");
            streamWriter.WriteLine(value.vertices);
            streamWriter.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ TRIANGLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            streamWriter.WriteLine(value.triangles);
            streamWriter.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            streamWriter.WriteLine("=======================================================================================================================================================================================================================================================================");
            streamWriter.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        streamWriter.Flush();
    }

    public T LoadFile()
    {
        string destination = Application.persistentDataPath + "/data.gielis";
        FileStream file = null;

        if (File.Exists(destination))
            file = File.OpenRead(destination);
        else
            Debug.LogError("No saving file to retrieve");

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        T log = (T) binaryFormatter.Deserialize(file);
        file.Close();

        return log;
    }
}
