﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public struct vec3
{
    public float x;
    public float y;
    public float z;
}

[Serializable]
public class ParametricSuperShapeLog
{
    public int resolution;

    public float a;
    public float b;
    public float m;
    public float n1;
    public float n2;
    public float n3;

    public Vector3[] vertices;
    public int[] triangles;

    public ParametricSuperShapeLog(float a, float b, float m, float n1, float n2, float n3, int resolution, Vector3[] vertices, int[] triangles)
    {
        this.resolution = resolution;

        this.a = a;
        this.b = b;
        this.m = m;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;

        this.vertices = vertices;

        this.triangles = triangles;
    }
}

[Serializable]
public class GeneralizedSuperShapeLog
{
    public int resolution;

    public float a;
    public float b;
    public float theta;
    public float delta;
    public float n1;
    public float n2;
    public float n3;

    public Vector3[] vertices;
    public int[] triangles;

    public GeneralizedSuperShapeLog(float a, float b, float theta, float delta, float n1, float n2, float n3, int resolution, Vector3[] vertices, int[] triangles)
    {
        this.resolution = resolution;

        this.a = a;
        this.b = b;
        this.theta = theta;
        this.delta = delta;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;

        this.vertices = vertices;

        this.triangles = triangles;
    }
}

[Serializable]
public class SuperShape4DLog
{
    public int resolution;

    public float a;
    public float b;
    public float m;
    public float n1;
    public float n2;
    public float n3;
    public float a2;
    public float b2;
    public float m2;
    public float n12;
    public float n22;
    public float n32;

    public Vector3[] vertices;
    public int[] triangles;

    public SuperShape4DLog(float a, float b, float m, float n1, float n2, float n3, float a2, float b2, float m2, float n12, float n22, float n32, int resolution, Vector3[] vertices, int[] triangles)
    {
        this.resolution = resolution;

        this.a = a;
        this.b = b;
        this.m = m;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        this.a2 = a2;
        this.b2 = b2;
        this.m2 = m2;
        this.n12 = n12;
        this.n22 = n22;
        this.n32 = n32;

        this.vertices = vertices;

        this.triangles = triangles;
    }
}

[Serializable]
public class SuperShapeLogs
{
    public int resolution;

    public float a;
    public float b;
    public float m;
    public float n1;
    public float n2;
    public float n3;

    public vec3[] vertices;
    public int[] triangles;

    public SuperShapeLogs(float a, float b, float m, float n1, float n2, float n3, int resolution, Vector3[] vertices, int[] triangles)
    {
        this.resolution = resolution;

        this.a = a;
        this.b = b;
        this.m = m;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;

        this.vertices = new vec3[vertices.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            this.vertices[i].x = vertices[i].x;
            this.vertices[i].y = vertices[i].y;
            this.vertices[i].z = vertices[i].z;
        }

        this.triangles = triangles;
    }
}
