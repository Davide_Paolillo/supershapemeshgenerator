﻿using UnityEngine;

public class TriangleMeshGenerator : MonoBehaviour
{
    private static Vector3[] GetTriangleFromVertices(int width, int height)
    {
        var vertices = new Vector3[4]
        {
            new Vector3(0, 0, 0),
            new Vector3(width, 0, 0),
            new Vector3(0, height, 0),
            new Vector3(width, height, 0)
        };

        return vertices;
    }

    private static Mesh GenerateMesh()
    {
        return new Mesh();
    }

    private static Vector3[] GenerateStaticNormals()
    {
        Vector3[] normals = new Vector3[4]
        {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward
        };

        return normals;
    }

    private static Vector2[] GetTextureCoordinates()
    {
        Vector2[] uv = new Vector2[4]
        {
              new Vector2(0, 0),
              new Vector2(1, 0),
              new Vector2(0, 1),
              new Vector2(1, 1)
        };

        return uv;
    }

    private static int[] GetTrianglesForQuad()
    {
        int[] tris = new int[6]
        {
            // lower left triangle
            0, 2, 1,
            // upper right triangle
            2, 3, 1
        };

        return tris;
    }

    public static Mesh GetTriangleMeshFromVertices(int width, int height)
    {
        Mesh myMesh = GenerateMesh();

        myMesh.vertices = GetTriangleFromVertices(width, height);
        myMesh.normals = GenerateStaticNormals();
        myMesh.uv = GetTextureCoordinates();

        return myMesh;
    }

    public static Mesh GetQuadMesh(int width, int height)
    {
        Mesh myMesh = GetTriangleMeshFromVertices(width, height);

        myMesh.triangles = GetTrianglesForQuad();

        return myMesh;
    }
}
