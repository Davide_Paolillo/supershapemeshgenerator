﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParametricSuperShape3D : MonoBehaviour
{
    [Header("Shape precision")]
    [Range(2, 256)] [SerializeField] private int resolution = 60;

    [Header("Shape Parameters")]
    [Range(0.0f, 100.0f)] [SerializeField] private float a = 1.0f;
    [Range(0.0f, 100.0f)] [SerializeField] private float b = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float m = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n1 = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n2 = 1.0f;
    [Range(-1000.0f, 100.0f)] [SerializeField] private float n3 = 1.0f;

    // Saving the meshes in the editor, don't need to show it
    [SerializeField, HideInInspector] private MeshFilter[] meshFilters;
    private SuperShape[] cubeFaces;
    private GameObject meshObject;

    public float A { get => a; set => a = value; }
    public float B { get => b; set => b = value; }
    public float M { get => m; set => m = value; }
    public float N1 { get => n1; set => n1 = value; }
    public float N2 { get => n2; set => n2 = value; }
    public float N3 { get => n3; set => n3 = value; }

    /*
    private void OnValidate()
    {
        Initialize();
        GenerateMesh();
    }
    */

    private void Start()
    {
        meshObject = this.transform.GetChild(0).gameObject;
        meshObject.AddComponent<MeshCollider>().convex = true;
        InitializeInUnpdate();
    }

    // Uncomment to draw the shape with random values
    /*
    private void Update()
    {
        GenerateMeshWithTime();
    }
    */

    private void Initialize()
    {
        this.transform.name = "ParametricSuperShape3D";

        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("Mesh");
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void InitializeInUnpdate()
    {
        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void GenerateMesh()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructMesh(a, b, m, n1, n2, n3);
        }
    }

    private void GenerateMeshWithTime()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructMesh(a, b, Time.time, n1, n2, n3);
        }
    }

    private void GenerateMeshWithSin()
    {
        float sinValue = Mathf.Sin(Time.time) * 10.0f;
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructMesh(a, b, sinValue, n1, n2, n3);
        }
    }

    public void GenerateMeshWithParameters(float a, float b, float m, float n1, float n2, float n3)
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructMesh(a, b, m, n1, n2, n3);
        }
    }
}
