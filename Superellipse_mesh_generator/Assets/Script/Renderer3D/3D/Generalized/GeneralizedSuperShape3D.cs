﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralizedSuperShape3D : MonoBehaviour
{
    [Header("Shape precision")]
    [Range(2, 256)] [SerializeField] private int resolution = 60;

    [Header("Shape")]
    [Range(0.0f, 100.0f)] [SerializeField] private float a = 1.0f;
    [Range(0.0f, 100.0f)] [SerializeField] private float b = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n1 = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n2 = 1.0f;
    [Range(-1000.0f, 100.0f)] [SerializeField] private float n3 = 1.0f;

    [Header("Curvature")]
    [Range(0.0f, 1000.0f)] [SerializeField] private float zeta = 1.0f;
    [Range(0.0f, 1000.0f)] [SerializeField] private float delta = 1.0f;

    // Saving the meshes in the editor, don't need to show it
    [SerializeField, HideInInspector] private MeshFilter[] meshFilters;
    private SuperShape[] cubeFaces;
    private GameObject meshObject;

    public float A { get => a; set => a = value; }
    public float B { get => b; set => b = value; }
    public float N1 { get => n1; set => n1 = value; }
    public float N2 { get => n2; set => n2 = value; }
    public float N3 { get => n3; set => n3 = value; }
    public float Zeta { get => zeta; set => zeta = value; }
    public float Delta { get => delta; set => delta = value; }

    // Turn on to see the shape drawned in the editor
    /*
    private void OnValidate()
    {
        Initialize();
        GenerateMesh();
    }*/

    private void Start()
    {
        meshObject = this.transform.GetChild(0).gameObject;
        meshObject.AddComponent<MeshCollider>().convex = true;
        InitializeInStart();
    }

    // Turn On to see the shape updating with random parameters
    /*
    private void Update()
    {
        GenerateMeshWithSin();
    }
    */

    private void Initialize()
    {
        this.transform.name = "GeneralizedSuperShape3D";

        if (meshFilters == null)
        {
            // want to render a quad in order to build a sphere
            meshFilters = new MeshFilter[1];
        }

        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // We avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("Mesh");
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each cube face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void InitializeInStart()
    {
        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void GenerateMesh()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructGeneralizedMesh(a, b, delta, zeta, n1, n2, n3);
        }
    }

    private void GenerateMeshWithTime()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructGeneralizedMesh(a, b, Time.time, Time.fixedTime, n1, n2, n3);
        }
    }

    private void GenerateMeshWithSin()
    {
        float timeSinValue = Mathf.Sin(Time.time) * 10.0f;
        float fixedTimeSinValue = Mathf.Sin(Time.fixedTime) * 10.0f;
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructGeneralizedMesh(a, b, timeSinValue, fixedTimeSinValue, n1, n2, n3);
        }
    }

    public void GenerateMeshWithParameters(float a, float b, float delta, float zeta, float n1, float n2, float n3)
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructGeneralizedMesh(a, b, delta, zeta, n1, n2, n3);
        }
    }
}
