﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperEgg : MonoBehaviour
{
    [Header("Shape precision")]
    [Range(2, 256)] [SerializeField] private int resolution = 60;

    [Header("Shape Parameters")]
    [Range(0.0f, 100.0f)] [SerializeField] private float a = 3f;
    [Range(0.0f, 100.0f)] [SerializeField] private float b = 3f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float c = 4f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float t = 2.5f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float r = 2f;

    // Saving the meshes in the editor, don't need to show it
    [SerializeField, HideInInspector] private MeshFilter[] meshFilters;
    private SuperShape[] cubeFaces;
    private GameObject meshObject;

    public float A { get => a; set => a = value; }
    public float B { get => b; set => b = value; }
    public float C { get => c; set => c = value; }
    public float T { get => t; set => t = value; }
    public float R { get => r; set => r = value; }

    /*
   // Draw in the editor
   private void OnValidate()
   {
       Initialize();
       GenerateMesh();
   }*/

    private void Start()
    {
        meshObject = this.transform.GetChild(0).gameObject;
        meshObject.AddComponent<MeshCollider>().convex = true;
        InitializeInUnpdate();
        GenerateMesh();
    }

    private void Initialize()
    {
        this.transform.name = "SuperEgg";

        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("PietHeinTribute");
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
                meshObject.transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void InitializeInUnpdate()
    {
        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void GenerateMesh()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructSuperEgg(a, b, c, r, t);
        }
    }

    private void GenerateMeshWithTime()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructSuperEgg(a, b, c, Time.time, Time.fixedTime);
        }
    }

    private void GenerateMeshWithSin()
    {
        float sinValue = Mathf.Sin(Time.time) * 10.0f;
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructSuperEgg(a, b, c, sinValue, Time.time);
        }
    }

    public void GenerateMeshWithParameters(float a, float b, float c, float r, float t)
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructSuperEgg(a, b, c, r, t);
        }
    }
}
