﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperShape4D : MonoBehaviour
{
    [Header("Shape precision")]
    [Range(2, 256)] [SerializeField] private int resolution = 60;

    [Header("First Shape")]
    [Range(0.0f, 100.0f)] [SerializeField] private float a = 1.0f;
    [Range(0.0f, 100.0f)] [SerializeField] private float b = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float m = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n1 = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float n2 = 1.0f;
    [Range(-1000.0f, 100.0f)] [SerializeField] private float n3 = 1.0f;

    [Header("Second Shape")]
    [Range(0.0f, 100.0f)] [SerializeField] private float sa = 1.0f;
    [Range(0.0f, 100.0f)] [SerializeField] private float sb = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sm = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sn1 = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sn2 = 1.0f;
    [Range(-1000.0f, 100.0f)] [SerializeField] private float sn3 = 1.0f;

    // Saving the meshes in the editor, don't need to show it
    [SerializeField, HideInInspector] private MeshFilter[] meshFilters;
    private SuperShape[] cubeFaces;
    private GameObject meshObject;

    public float A { get => a; set => a = value; }
    public float B { get => b; set => b = value; }
    public float M { get => m; set => m = value; }
    public float N1 { get => n1; set => n1 = value; }
    public float N2 { get => n2; set => n2 = value; }
    public float N3 { get => n3; set => n3 = value; }
    public float SA { get => sa; set => sa = value; }
    public float SB { get => sb; set => sb = value; }
    public float SM { get => sm; set => sm = value; }
    public float SN1 { get => sn1; set => sn1 = value; }
    public float SN2 { get => sn2; set => sn2 = value; }
    public float SN3 { get => sn3; set => sn3 = value; }

    // Draw in the editor
    /*
    private void OnValidate()
    {
        Initialize();
        GenerateMesh();
    }
    */

    private void Start()
    {
        meshObject = this.transform.GetChild(0).gameObject;
        meshObject.AddComponent<MeshCollider>().convex = true;
        InitializeInUnpdate();
    }

    // Uncomment to draw figure with random value
    /*
    private void Update()
    {
        GenerateMeshWithTime();
    }
    */

    private void Initialize()
    {
        this.transform.name = "SuperShape4D";

        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("Mesh");
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void InitializeInUnpdate()
    {
        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }
    }

    private void GenerateMesh()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, m, n1, n2, n3, sa, sb, sm, sn1, sn2, sn3);
        }
    }

    private void GenerateMeshWithTime()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, Time.time % 100, n1, n2, n3, sa, sb, Time.fixedTime % 100, sn1, sn2, sn3);
        }
    }

    private void GenerateMeshWithSin()
    {
        float timeSinValue = Mathf.Sin(Time.time) * 10.0f;
        float fixedTimeSinValue = Mathf.Sin(Time.fixedTime) * 10.0f;

        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, timeSinValue, n1, n2, n3, sa, sb, fixedTimeSinValue, sn1, sn2, sn3);
        }
    }

    public void GenerateMeshWithParameters(float a, float b, float m, float n1, float n2, float n3, float sa, float sb, float sm, float sn1, float sn2, float sn3)
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, m, n1, n2, n3, sa, sb, sm, sn1, sn2, sn3);
        }
    }
}
