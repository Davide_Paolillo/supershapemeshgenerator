﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperEllipse : MonoBehaviour
{
    // Number of points that indicates the precision at wich we want to design out circumference
    [Range(20, 100)][SerializeField] private int numberOfPoints = 20;

    // Superellipse double radiuses
    [Range(0.0f, 10.0f)][SerializeField] private float horizontalRadius;
    [Range(0.0f, 10.0f)] [SerializeField] private float verticalRadius;

    // Additional constraints
    [Range(0.0f, 2.0f)] [SerializeField] private float n1 = 1.0f;
    [Range(0.0f, 2.0f)] [SerializeField] private float n2 = 1.0f;
    [Range(0.0f, 2.0f)] [SerializeField] private float n3 = 1.0f;
    [Range(0.0f, 10.0f)] [SerializeField] private float m = 5.0f;

    // How many rotations is the 
    [Range(0, 40)] [SerializeField] private int piExtension = 12;

    // Factor that indeicates the curvature of the closed curves between horizontal radius and vertical radiuse
    [Range(0.0f, 5.0f)] [SerializeField] private float n = 0.0f;

    // List of points in wich we will store out "drawing points"
    private List<Vector3> points;

    // Debug renderer to visualize the cinrcumference in 2D
    private LineRenderer lineRenderer;

    private bool maxSimulationPosition;

    void Start()
    {
        points = new List<Vector3>();

        lineRenderer = GetComponent<LineRenderer>();

        maxSimulationPosition = true;

        CreateSuperShape();
    }

    void Update()
    {
        CreateSuperShape();
    }

    private void SimulateSuperEllipseMoving()
    {
        if (n < 5.0f && maxSimulationPosition)
        {
            n += 1.0f * Time.deltaTime;
        }
        else if (n > 0.2f)
        {
            maxSimulationPosition = false;
            n -= 1.0f * Time.deltaTime;
        }
        else
        {
            maxSimulationPosition = true;
        }
    }

    /// <summary>
    /// Create a superellipse with the given horizontalRadius and verticalRadius, starting from the number of points.
    /// </summary>
    private void CreateSuperEllispe()
    {
        // Pi indicates a semi-circumference, so we get the entire circle and dividng that to the number of points to get how much we should increment between each angle
        float angleIncrementFactor = (Mathf.PI * 2) / numberOfPoints;
        // The starting angle from wich we start to draw our circumference
        float startAngle = 0.0f;

        // Iterating through each point in order to draw every bit of the circumference, more points means more precision but also more computational complexity
        for (int i = 0; i <= numberOfPoints; i++)
        {
            // Getting the position of the x and y points of the line to render (parametric equations of a 2D superellipse)
            float x_pos = Mathf.Pow(Mathf.Abs(Mathf.Cos(startAngle)), 2/n) * horizontalRadius * Mathf.Sign(Mathf.Cos(startAngle));
            float y_pos = Mathf.Pow(Mathf.Abs(Mathf.Sin(startAngle)), 2/n) * verticalRadius * Mathf.Sign(Mathf.Sin(startAngle));

            // Adding to our points the vector that defines the circumference
            points.Add(new Vector3(x_pos, y_pos, 10.0f));

            // Increment the angle by the factor that we defined (the angle is in radiants not in grades)
            startAngle += angleIncrementFactor;
        }

        // TODO: Create a mesh instead of a line renderer, a mesh with all triangle points needed!

        // Setting up our debug line renderer
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());

        // Clearing list of points, as long as we don't need it anymore
        points.Clear();
    }

    /// <summary>
    /// Compute a supershape radius with a starting angle in radiants
    /// </summary>
    /// <param name="angleInRadiants">The angle in radiants</param>
    /// <returns></returns>
    private float ComputeSuperShape(float angleInRadiants)
    {
        float part1 = (1 / horizontalRadius) * Mathf.Cos(angleInRadiants * m  / 4);
        part1 = Mathf.Abs(part1);
        part1 = Mathf.Pow(part1, n2);

        float part2 = (1 / verticalRadius) * Mathf.Sin(angleInRadiants * m / 4);
        part2 = Mathf.Abs(part2);
        part2 = Mathf.Pow(part2, n3);

        // That means under square of n1
        float part3 = Mathf.Pow(part1 + part2, 1/n1);

        if(part3 == 0)
        {
            return 0;
        }

        return (1 / part3);
    }
    
    /// <summary>
    /// Create a supershape with the given horizontalRadius and verticalRadius, starting from the number of points.
    /// </summary>
    private void CreateSuperShape()
    {
        // Pi indicates a semi-circumference, so we get the entire circle and dividng that to the number of points to get how much we should increment between each angle
        float angleIncrementFactor = (Mathf.PI * piExtension) / numberOfPoints;
        // The starting angle from wich we start to draw our circumference
        float startAngle = 0.0f;

        // Iterating through each point in order to draw every bit of the circumference, more points means more precision but also more computational complexity
        for (int i = 0; i <= numberOfPoints; i++)
        {
            // Getting the computed radius
            float r = ComputeSuperShape(startAngle);

            // Getting the position of the x and y points of the line to render (parametric equations of a 2D superellipse)
            float x_pos = r * Mathf.Cos(startAngle);
            float y_pos = r * Mathf.Sin(startAngle);

            // Adding to our points the vector that defines the circumference
            points.Add(new Vector3(x_pos, y_pos, 10.0f));

            // Increment the angle by the factor that we defined (the angle is in radiants not in grades)
            startAngle += angleIncrementFactor;
        }

        // TODO: Create a mesh instead of a line renderer, a mesh with all triangle points needed!

        // Setting up our debug line renderer
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());

        // Clearing list of points, as long as we don't need it anymore
        points.Clear();
    }
}
